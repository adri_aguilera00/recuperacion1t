using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
  
    public void Jugar()
    {
        SceneManager.LoadScene("Recuperación");
    }

    public void ReiniciarNivel()
    {
        SceneManager.LoadScene("Recuperación");
    }

    public void SalirJuego()
    {
        Application.Quit();
    }
}
