using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudController : MonoBehaviour

{
    public GameObject Nube;
    public bool NubesSaliendo = true;

    private void Start()
    {
        StartCoroutine(UsingYield(1));
    }
    // Update is called once per frame
    void Update()
    {
       
    }
    IEnumerator UsingYield(int seconds)
    {
        
        while (NubesSaliendo == true)
        {
            yield return new WaitForSeconds(2);
            Instantiate(Nube, transform.position = new Vector3(0,-2.5f, 0), Quaternion.identity);
        }
    }
}
