using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoNube : MonoBehaviour
{
    public int VelocidadNube;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(-8, -2.5f, 0), VelocidadNube * Time.deltaTime);
        if (transform.position == new Vector3(-8, -2.5f, 0))
        {
            Destroy(gameObject);
        }
    }

}
